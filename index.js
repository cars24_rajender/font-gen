const path = require('path');
const fs = require('fs');
const webfontsGenerator = require('webfonts-generator');
const iconsDir = "icons"
const output = "output"

const directoryPath = path.join(__dirname, iconsDir);
fs.readdir(directoryPath, function (err, files) {
    if (err) {
        return console.log('Unable to scan directory: ' + err);
    }
    webfontsGenerator({
        files: files.map(file => [iconsDir, file].join(path.sep)),
        dest: output,
        }, function(error) {
        if (error) {
            console.log('Fail!', error);
        } else {
            console.log('Done!');
        }
    })
});