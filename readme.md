# font-gen
Place all the svg files inside the icons folder and run `node index.js` to build your font file. You'll find the output files in `output` directory.

## Installation
```js
yarn install
```

## Build
```
node index.js
```
